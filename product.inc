<?php

/**
 * @file
 *   Commerce Product migration.
 *   This is a dynamic migration, reused for every product type
 *   (so that products of each type can be imported separately)
 */

class CommerceMigrateUbercartProductMigration extends DynamicMigration {

  protected $file_public_path = "";
  protected $file_private_path = "";
  protected $file_temporary_path = "";
  protected $source_drupal_root = "";
  protected $image_fields = "";
  protected $translated = false;

  public function __construct(array $arguments) {
    $this->arguments = $arguments;
    parent::__construct();
    $this->description = t('Import products from Ubercart.');
    $this->dependencies = array('CommerceMigrateUbercartProductType');
    $this->image_fields = preg_split('/ *[, ]+ */', variable_get('commerce_migrate_ubercart_image_fields', 'field_image_cache'));

    $this->file_public_path = variable_get('commerce_migrate_ubercart_public_files_directory', variable_get('file_public_path', 'sites/default/files'));
    $this->source_drupal_root = variable_get('commerce_migrate_ubercart_source_drupal_root', DRUPAL_ROOT);

    // Is translation enabled and is content translation enabled for the content type?
    // @todo: Support cross-database migrations
    if (module_exists('i18n_node') && translation_supported_type($arguments['type'])) {
      $this->translated = TRUE;
    }

    // Create a map object for tracking the relationships between source rows
    $this->map = new MigrateSQLMap($this->machineName,
      array(
          'nid' => array(
            'type' => 'int',
            'unsigned' => TRUE,
            'not null' => TRUE,
            'description' => 'Ubercart node ID',
            'alias' => 'ucp',
          ),
      ),
      MigrateDestinationEntityAPI::getKeySchema('commerce_product', $arguments['type'])
    );
    // Create a MigrateSource object, which manages retrieving the input data.
    $connection = commerce_migrate_ubercart_get_source_connection();
    $query = $connection->select('node', 'n');
    $query->innerJoin('uc_products', 'ucp', 'n.nid = ucp.nid AND n.vid = ucp.vid');
    $query->fields('n', array('nid', 'tnid', 'vid', 'type', 'title', 'created', 'changed'))
          ->fields('ucp', array('model', 'sell_price'))
          ->condition('n.type', $arguments['type'])
          ->distinct();
    if (variable_get('commerce_migrate_ubercart_user_map_ok', FALSE)) {
      $query->addField('n', 'uid', 'uid');
    }

    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));

    $this->destination = new MigrateDestinationEntityAPI('commerce_product', $arguments['type']);

    // Properties
    $this->addFieldMapping('sku', 'model')
      ->dedupe('commerce_product', 'sku');
    $this->addFieldMapping('type', 'type')
      ->sourceMigration('CommerceMigrateUbercartProductType');
    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('changed', 'changed');
    // Default uid to 0 if we're not mapping it.
    if (variable_get('commerce_migrate_ubercart_user_map_ok', FALSE) && variable_get('commerce_migrate_ubercart_user_migration_class', "") != "") {
      $this->addFieldMapping('uid', 'uid')->sourceMigration(variable_get('commerce_migrate_ubercart_user_migration_class', ""))->defaultValue(0);
    }
    else {
      $this->addFieldMapping('uid', 'uid')->defaultValue(0);
    }
    $this->addFieldMapping('status')
      ->defaultValue(1);
    $this->addFieldMapping('product_id')->issueGroup(t('DNM'));
    $this->addFieldMapping('path')->issueGroup(t('DNM'));
    // Fields
    $this->addFieldMapping('commerce_price', 'sell_price');
    $this->addFieldMapping('field_commerce_file_s3', 'commerce_file_s3')
      ->arguments(array(
        'skip_source_file_check' => TRUE,
        'preserve_files' => TRUE,
        'file_function' => 'file_link',
      ));
    // @todo: This entire section should be overridden in a subclass
    if (module_exists('commerce_migrate_uc_file_s3')) {
      $this->addFieldMapping('field_commerce_file_s3', 'commerce_file_s3')
        ->arguments(array(
          'skip_source_file_check' => TRUE,
          'preserve_files' => TRUE,
          'file_function' => 'file_link',
        ));
    }
    // Map tnid if nodes are translated
    // @todo: This should not be a field mapping, but has to be passed somehow to the prepareRow callback.
    if($this->translated){
      $this->addFieldMapping('tnid', 'tnid');
    }
  }

  public function prepareRow($row) {

    // Prepare the imagefields, from http://drupal.org/node/1159234
    $connection = commerce_migrate_ubercart_get_source_connection();
    
    // @todo: MUST be moved into a subclass!
    // module existence. But for now, this will have to do it.
    if (module_exists('commerce_migrate_uc_file_s3')) {
      // Prepare the commerce_file fields
      $query = $connection->select('uc_product_features', 'upf')
        ->condition('upf.fid', 'file_s3');
      $query->innerJoin('node', 'n', 'upf.nid = n.nid');
      $query->leftJoin('uc_file_s3_products', 'ufsp', 'upf.pfid = ufsp.pfid');
      $query->innerJoin('uc_files_s3', 'ufs', 'ufs.fid = ufsp.fid');
      $query->fields('n', array('nid', 'vid', 'type', 'title', 'created', 'changed'))
            ->fields('ufsp', array('model', 'description', 'download_limit', 'address_limit', 'time_quantity'))
            ->fields('ufs', array('filename'))
            ->distinct();
      $query->condition('n.nid', $row->nid)
        ->condition('n.vid', $row->vid);
      $result = $query->execute();
      foreach ($result as $file_row) {
        $path = 's3://' . $file_row->filename;
        $value = array(
          'path' => $path,
          'download_limit' => $file_row->download_limit,
          'address_limit' => $file_row->address_limit,
          'duration' => $file_row->time_quantity,
        );
        $row->commerce_file_s3[] = drupal_json_encode($value);
      }
    }
    
    // Is the current node the source translation?
    if ($this->translated && $row->nid != $row->tnid) {
      return FALSE;
    }
  }
  
  public function prepare($entity, $row) {
    
    //dsm($entity);
    
    $node = node_load($row->nid);
    
    
    $entity->field_isbn = $node->field_isbn;
    $entity->field_book_number = $node->field_sortiernummer;
    
    if (isset($node->field_erscheinungsjahr['und'][0]['value']))
    $entity->field_release_date['und'][0]['value'] = $node->field_erscheinungsjahr['und'][0]['value'] . '-01-01 00:00:00'; //date('Y-m-d H:i:s', strtotime($node->field_erscheinungsjahr['und'][0]['value']));
    
    $entity->field_edition = $node->field_auflage;
    
    if (count($node->field_format_us) && stristr($node->field_format_us['und'][0]['value'], 'in') && stristr($node->field_format_us['und'][0]['value'], ' x ') && !stristr($node->field_format_us['und'][0]['value'], 'Leporello')){
      $fractions = array( ' 1/2',
                          ' ½',
                          
                          ' 1/3',
                          ' 2/3',
                          ' 3/3',
                          
                          ' 1/4',
                          ' ¼',
                          ' 3/4',
                          ' ¾',
                          
                          ' 1/8',
                          ' 2/8',
                          ' 3/8',
                          ' 4/8',
                          ' 5/8',
                          ' 6/8',
                          ' 7/8',
      );
      $decimal = array(   '.5',
                          '.5',
                          
                          '.33333',
                          '.33333',
                          '.33333',
                          
                          '.25',
                          '.25',
                          '.75',
                          '.75',
                          
                          '.125',
                          '.25',
                          '.375',
                          '.5',
                          '.625',
                          '.75',
                          '.875',
      );
      
      $dimensions = str_replace($fractions, $decimal, $node->field_format_us['und'][0]['value']);
      $units = array(' inches', ' in.', ' in');
      $dimensions = str_replace($units, '', $dimensions);
      $dimensions = str_replace(' ', '', $dimensions);
      $dimensions = explode('x', $dimensions);
      $dimensions = array(
        'length' => $dimensions[0],
        'width' => $dimensions[1],
        'height' => 1,
        'unit' => 'in',
      );
      $entity->field_dimensions['und'][0] = $dimensions;
    }
    
    if(count($node->field_seitenzahl_deutsch)){
      $pattern = '/\d+/';
      $pages = $node->field_seitenzahl_deutsch['und'][0]['value'];
      preg_match($pattern, $pages, $matches);
      $entity->field_pages['und'][0]['value'] = $matches[0];
    }
    
    $entity->field_binding['und'][0]['tid'] = 41; // Hardcover
    
    if (count($node->field_rarebild)) {
      $entity->field_status['und'][0]['tid'] = 37; // Rare
    }
    else if (count($node->field_bild_limited)) {
      $entity->field_status['und'][0]['tid'] = 38; // Limited Edition
    }
    else {
      $entity->field_status['und'][0]['tid'] = 36; // Catalogue
    }
    
    if (count($node->field_80_81) && $node->field_80_81['und'][0]['value'] == '80') {
      $entity->field_series['und'][0]['tid'] = 40; // 80*81
    }
    
    $entity->field_tags = $node->taxonomyextra;
    
    $entity->field_book_cover = $node->field_coverbild;
    $entity->field_book_images = $node->field_innenbild;
    
    $entity->field_author = $node->field_autor;
    $entity->field_designer = $node->field_gestaltung;
    $entity->field_artist = $node->field_kuenstler;
    $entity->field_editor = $node->field_herausgeber;
    
    /*$entity->field_80_81 = $node->field_80_81;
    $entity->field_bild_limited = $node->field_bild_limited;
    $entity->field_limited_ja_nein = $node->field_limited_ja_nein;
    $entity->field_rarebild = $node->field_rarebild;
    $entity->field_vorschaubild = $node->field_vorschaubild;*/
    
    //dsm($row);
    //dsm($node);
    //dsm($entity);
  }
  
  public function postImport() {
    parent::postImport();
    
    // @todo: Support cross-database migrations
    $connection = commerce_migrate_ubercart_get_source_connection();
    $query_string = db_query('UPDATE {migrate_map_' . $this->generateMachineName() . '} mm
                       JOIN {node} n ON (mm.sourceid1 = n.nid)
                       JOIN {migrate_map_' . $this->generateMachineName() . '} mm2 ON (mm2.sourceid1 = n.tnid)
                       SET mm.destid1 = mm2.destid1, mm.destid2 = mm2.destid2');
    $result = $connection->query($query_string);
  }

   /**
   * Return a list of all product migrations.
   */
  public static function getProductMigrations() {
    $migrations = array();
    foreach (commerce_product_types() as $type => $product_type) {
      $migrations[] = 'CommerceMigrateUbercartProduct' . ucfirst($type);
    }
    return $migrations;
  }


  /**
   * Construct the machine name (identifying the migration in "drush ms" and other places).
   */
  protected function generateMachineName($class_name = NULL) {
    return 'CommerceMigrateUbercartProduct' . ucfirst($this->arguments['type']);
  }
}
